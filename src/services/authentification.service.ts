import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {Response} from "../models/Response";
import {Client} from "../models/Client";
import {JwtAuthenticationResponse} from "../models/JwtAuthenticationResponse";
import {Signin} from "../models/Signin";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(private httpClient: HttpClient) { }

  signIn(userSignIn: any): Observable<Response<JwtAuthenticationResponse>> {
    return this.httpClient.post<Response<JwtAuthenticationResponse>>('http://localhost:8010/auth/signin', userSignIn)
      .pipe(
        catchError(error => {
          console.error('Error connect user:', error);
          return throwError(error);
        })
      );
  }
  signUp(userSignUp: any): Observable<Response<JwtAuthenticationResponse>> {
    return this.httpClient.post<Response<JwtAuthenticationResponse>>('http://localhost:8010/auth/signup', userSignUp)
      .pipe(
        catchError(error => {
          console.error('Error connect user:', error);
          return throwError(error);
        })
      );
  }
}

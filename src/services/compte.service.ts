import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {Response} from "../models/Response";
import {Client} from "../models/Client";
import {Compte} from "../models/Compte";

@Injectable({
  providedIn: 'root'
})
export class CompteService {

  constructor(private httpClient: HttpClient) { }

  private getHeaders(): HttpHeaders {
    // Modify this function to get the authorization token from your authentication service
    const token = localStorage.getItem("token");

    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
  }
  GetAllCompte(): Observable<Response<Compte[]>> {
    return this.httpClient.get<Response<Compte[]>>('http://localhost:8010/compte', { headers: this.getHeaders() });
  }

  GetAllCompteByClient(cin: number): Observable<Response<Compte[]>> {
    return this.httpClient.get<Response<Compte[]>>(`http://localhost:8010/compte/compteByClient/${cin}`, { headers: this.getHeaders() });
  }
  saveCompte(CompteToSave: any): Observable<Response<Compte>> {
    // Removed headers setting for FormData
    return this.httpClient.post<Response<Compte>>('http://localhost:8010/compte', CompteToSave, { headers: this.getHeaders() })
      .pipe(
        catchError(error => {
          console.error('Error saving compte:', error);
          return throwError(error);
        })
      );
  }
  deleteCompte(rib: string): Observable<Response<Compte>> {
    return this.httpClient.delete<Response<Compte>>(`http://localhost:8010/compte/${rib}`, { headers: this.getHeaders() });
  }

  updateCompte(compteToUpdate: Compte, rib: string): Observable<Response<Compte>> {
    const url = `http://localhost:8010/compte/${rib}`;

    return this.httpClient.put<Response<Compte>>(url, compteToUpdate, { headers: this.getHeaders() })
      .pipe(
        catchError(error => {
          console.error('Error updating compte:', error);
          return throwError(error);
        })
      );
  }
  getCompteByRib(rib: string): Observable<Response<Compte>> {
    return this.httpClient.get<Response<Compte>>(`http://localhost:8010/compte/${rib}`, { headers: this.getHeaders() });
  }
}

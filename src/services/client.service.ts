import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {Client} from "../models/Client";
import {Response} from "../models/Response";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private httpClient: HttpClient) { }

  private getHeaders(): HttpHeaders {
    // Modify this function to get the authorization token from your authentication service
    const token = localStorage.getItem("token");

    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
  }
  GetAllClient(): Observable<Response<Client[]>> {
    return this.httpClient.get<Response<Client[]>>('http://localhost:8010/client', { headers: this.getHeaders() });
  }
  saveClient(ClientToSave: any): Observable<Response<Client>> {
    // Removed headers setting for FormData
    return this.httpClient.post<Response<Client>>('http://localhost:8010/client', ClientToSave, { headers: this.getHeaders() })
      .pipe(
        catchError(error => {
          console.error('Error saving client:', error);
          return throwError(error);
        })
      );
  }
  deleteClient(cin: number): Observable<Response<Client>> {
    return this.httpClient.delete<Response<Client>>(`http://localhost:8010/client/${cin}`, { headers: this.getHeaders() });
  }

  updateClient(clientToUpdate: Client, id: number): Observable<Response<Client>> {
    const url = `http://localhost:8010/client/${id}`;

    return this.httpClient.put<Response<Client>>(url, clientToUpdate, { headers: this.getHeaders() })
      .pipe(
        catchError(error => {
          console.error('Error updating region:', error);
          return throwError(error);
        })
      );
  }

  getClientByCin(clientCin: number): Observable<Response<Client>> {
    return this.httpClient.get<Response<Client>>(`http://localhost:8010/client/${clientCin}`, { headers: this.getHeaders() });
  }
  private apiUrl = 'http://localhost:8010/client';
  searchClients(term: string): Observable<Client[]> {
    // Appel à votre backend pour rechercher les clients
    const url = `${this.apiUrl}?term=${term}`;
    return this.httpClient.get<Client[]>(url);
  }

}

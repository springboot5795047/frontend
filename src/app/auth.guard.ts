import {CanActivate, CanActivateFn, Router} from '@angular/router';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(): boolean {
    if (localStorage.getItem('token')!=null) {
      return true; // Allow access to the route
    } else {
      this.router.navigate(['/Login']); // Redirect to the login page
      return false; // Block access to the route
    }
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashNavComponent } from './components/dash-nav/dash-nav.component';
import { DashSideComponent } from './components/dash-side/dash-side.component';
import { DashHomeComponent } from './components/dash-home/dash-home.component';
import { DashClientComponent } from './components/dash-client/dash-client.component';
import { DashCompteComponent } from './components/dash-compte/dash-compte.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import { ConfirmDialogClientComponent } from './components/dash-client/confirm-dialog-client/confirm-dialog-client.component';
import { MatDialogModule} from "@angular/material/dialog";
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {AsyncPipe, NgOptimizedImage} from "@angular/common";
import { AjoutCompteComponent } from './components/dash-compte/ajout-compte/ajout-compte.component';
import { DashLoginComponent } from './components/dash-login/dash-login.component';
import { DashRegisterComponent } from './components/dash-register/dash-register.component';
import {MatCardModule} from "@angular/material/card";
import { DashCompteClientComponent } from './components/dash-compte-client/dash-compte-client.component';

@NgModule({
  declarations: [
    AppComponent,
    DashNavComponent,
    DashSideComponent,
    DashHomeComponent,
    DashClientComponent,
    DashCompteComponent,
    ConfirmDialogClientComponent,
    AjoutCompteComponent,
    DashLoginComponent,
    DashRegisterComponent,
    DashCompteClientComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatTableModule,
        HttpClientModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatDialogModule,
        SweetAlert2Module.forRoot(),
        MatFormFieldModule,
        FormsModule,
        MatAutocompleteModule,
        AsyncPipe,
        NgOptimizedImage,
        MatCardModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

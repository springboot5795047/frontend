import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthentificationService} from "../../../services/authentification.service";
import {LocationStrategy} from "@angular/common";
import {SweetAlert2LoaderService} from "@sweetalert2/ngx-sweetalert2";

@Component({
  selector: 'app-dash-register',
  templateUrl: './dash-register.component.html',
  styleUrls: ['./dash-register.component.css']
})
export class DashRegisterComponent implements OnInit{
  form!:FormGroup;
  initForm(): void {
    this.form = new FormGroup({
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
    });
  }
  constructor(private router : Router,private AS: AuthentificationService, private locationStrategy: LocationStrategy,private swalService: SweetAlert2LoaderService) {}
  ngOnInit() {
    this.initForm();
  }
  register() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
    const data = {
      firstName:this.form.value.firstName,
      lastName:this.form.value.lastName,
      email: this.form.value.email,
      password: this.form.value.password
    };

    this.AS.signUp(data).subscribe(
      (x) => {
        console.log(x.data.token);
        localStorage.setItem("token",x.data.token);
        this.router.navigate(['/Login']);
      },
      (error) => {
        console.error('Error saving compte:', error);
        this.form.controls['name'].reset();
      }
    );
  }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashRegisterComponent } from './dash-register.component';

describe('DashRegisterComponent', () => {
  let component: DashRegisterComponent;
  let fixture: ComponentFixture<DashRegisterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DashRegisterComponent]
    });
    fixture = TestBed.createComponent(DashRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {ClientService} from "../../../services/client.service";
import {CompteService} from "../../../services/compte.service";
import {Client} from "../../../models/Client";
import {Compte} from "../../../models/Compte";

@Component({
  selector: 'app-dash-home',
  templateUrl: './dash-home.component.html',
  styleUrls: ['./dash-home.component.css']
})
export class DashHomeComponent implements OnInit{
  clients!: Client[];
  comptes!: Compte[];
  email:String="";
  nbClient=0;
  nbCompte=0;
  ngOnInit(): void {
    setTimeout(() => {
      // Function to read the value from localStorage
      this.email = localStorage.getItem("email") ?? "";
    }, 200);

    this.ClS.GetAllClient().subscribe(response => {
        this.clients = response.data;
        this.nbClient = this.clients.length;
    });

    this.CS.GetAllCompte().subscribe(response => {
      this.comptes = response.data;
      this.nbCompte = this.comptes.length;
    });



  }
  constructor(private ClS: ClientService, private CS: CompteService) {
  }
}

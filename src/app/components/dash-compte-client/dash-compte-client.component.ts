import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Client} from "../../../models/Client";
import {ClientService} from "../../../services/client.service";
import {Compte} from "../../../models/Compte";
import {CompteService} from "../../../services/compte.service";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-dash-compte-client',
  templateUrl: './dash-compte-client.component.html',
  styleUrls: ['./dash-compte-client.component.css']
})
export class DashCompteClientComponent implements OnInit{
  cin!: string;
  client!: Client;
  comptes!: Compte[];
  displayedColumns: string[] = ['rib','solde'];
  dataSource!: MatTableDataSource<Compte>;
  constructor(private route: ActivatedRoute, private  ClS: ClientService,private CS: CompteService,) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      // Retrieve the value of the 'cin' parameter
      this.cin = params['cin'];

      // Now you can use the 'cin' value in your component logic
      console.log('CIN:', this.cin);
      this.ClS.getClientByCin(parseInt(this.cin)).subscribe(response => {
        this.client = response.data;
      });
    });
    this.dataSource = new MatTableDataSource<Compte>();
    this.initializeTab();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  initializeTab() {
    this.CS.GetAllCompteByClient(parseInt(this.cin)).subscribe((response) => {
      this.comptes = response.data;
      this.dataSource.data = this.comptes;
    });
  }

}

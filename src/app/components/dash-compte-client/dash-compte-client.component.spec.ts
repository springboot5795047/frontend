import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashCompteClientComponent } from './dash-compte-client.component';

describe('DashCompteClientComponent', () => {
  let component: DashCompteClientComponent;
  let fixture: ComponentFixture<DashCompteClientComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DashCompteClientComponent]
    });
    fixture = TestBed.createComponent(DashCompteClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { animate, state, style, transition, trigger } from "@angular/animations";
import { Client } from "../../../models/Client";
import { Router } from "@angular/router";
import { ClientService } from "../../../services/client.service";
import { Response } from "../../../models/Response";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LocationStrategy} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmDialogClientComponent} from "./confirm-dialog-client/confirm-dialog-client.component";
import {SweetAlert2LoaderService} from "@sweetalert2/ngx-sweetalert2";
import Swal from "sweetalert2";


@Component({
  selector: 'app-dash-client',
  templateUrl: './dash-client.component.html',
  styleUrls: ['./dash-client.component.css'],

})
export class DashClientComponent implements OnInit {
  form!:FormGroup;
  clients!: Client[];
  client!: Client;
  displayedColumns: string[] = ['cin', 'nom', 'prenom','actions'];

  dataSource!: MatTableDataSource<Client>;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  initializeTab() {
    this.CS.GetAllClient().subscribe((response) => {
      console.log(response);
      this.clients = response.data;
      this.dataSource.data = this.clients;
    });
  }
  initForm(): void {
    this.form = new FormGroup({
      cin: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^.{8}$/), // Pattern for 8 digits
      ]),
      nom: new FormControl(null, [Validators.required]),
      prenom: new FormControl(null, [Validators.required]),
      cinDisabled: new FormControl({value: null, disabled: true}, [Validators.required]),
      nomEdit: new FormControl(null, [Validators.required]),
      prenomEdit: new FormControl(null, [Validators.required]),
    });
  }

  constructor(private CS: ClientService, private locationStrategy: LocationStrategy, private dialog:MatDialog,private swalService: SweetAlert2LoaderService,private router:Router) {}

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Client>();
    this.initializeTab();
    this.initForm();
  }
  onsub(): void {
    const cinControl = this.form.get('cin');
    const nomControl = this.form.get('nom');
    const prenomControl = this.form.get('prenom');
    this.form.markAllAsTouched();
    if (cinControl && cinControl.valid) {
      const cin = cinControl.value;
        if(nomControl && nomControl.valid && prenomControl && prenomControl.valid) {
          // Check if the client with the given cin already exists
          this.CS.getClientByCin(cin).subscribe(
            (response) => {
              console.log('Response:', response);

              if (response.code == 200) {
                // Client exists, show SweetAlert
                Swal.fire({
                  title: 'Client existe déjà',
                  text: 'Le client avec le numéro CIN spécifié existe déjà.',
                  icon: 'warning',
                  confirmButtonText: 'OK',
                });
                this.form.controls['cin'].reset();
                this.form.controls['nom'].reset();
                this.form.controls['prenom'].reset();
              } else {
                // Client doesn't exist, proceed with saving
                const data = {
                  cin: cin,
                  nom: this.form.value.nom,
                  prenom: this.form.value.prenom
                };

                // Save the client
                this.CS.saveClient(data).subscribe(
                  (x) => {
                    // Successfully saved, navigate back and reload
                    this.locationStrategy.back();
                    Swal.fire({
                        title: 'Client ajouté avec succés !',
                        icon: 'success',
                        timer: 4000,
                        showConfirmButton: false
                      }
                    );
                    /*setTimeout(() => {
                      this.router.navigate(['/ClientDash']);
                      window.location.reload();
                    }, 2000);*/
                    window.location.reload();
                    this.form.reset();
                  },

                  (saveError) => {
                    console.error('Error saving client:', saveError);
                    this.form.controls['name'].reset();
                  }
                );
              }
            },
            (error) => {
              // Some error occurred, handle accordingly
              console.error('Error checking client existence:', error);
            }
          );
        }
    }
    else{
      console.log('Form is not valid');
      this.form.controls['cin'].reset();
      Swal.fire({
        title: 'Numéro CIN incorrecte !',
        text: 'Le CIN doit comporter exactement 8 chiffres et ne doit contenir que des chiffres.',
        icon: 'warning',
        confirmButtonText: 'OK',
        showConfirmButton: false
      });
    }
  }


  update() {
    // Mark all form fields as touched
    this.form.markAllAsTouched();
    const nomControl = this.form.get('nomEdit');
    const prenomControl = this.form.get('prenomEdit');
    if (nomControl && nomControl.valid && prenomControl && prenomControl.valid) {
      const data = {
        cin: this.form.value.cin,
        nom: this.form.value.nomEdit,
        prenom: this.form.value.prenomEdit
      };

      this.CS.updateClient(data, data.cin).subscribe(
        () => {
          this.locationStrategy.back();
          Swal.fire({
            title: 'Client mis à jour avec succès !',
            icon: 'success',
            timer: 7000,
            showConfirmButton: false
          });
          window.location.reload();
        },
        (error) => {
          // Handle the error accordingly, you may want to show an error message or log the error.
          console.error('Error updating client:', error);
          this.form.reset();  // Reset the form if there is an error
        }
      );
    }
  }

  open(cin:number){
    this.CS.getClientByCin(cin).subscribe(client => {
      this.client = client.data;
      console.log(this.client);
      this.form.controls['nomEdit'].setValue(this.client.nom);
      this.form.controls['prenomEdit'].setValue(this.client.prenom);
      this.form.controls['cin'].setValue(this.client.cin);
      this.form.controls['cinDisabled'].setValue(this.client.cin);
    });
  }

  deleteClient(cin: number) {
    Swal.fire({
      title: 'Vous êtes sûr ?',
      text: 'Voulez-vous vraiment supprimer ce client ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',
    }).then((result) => {
      if (result.isConfirmed) {
        this.CS.deleteClient(cin).subscribe(
          (response) => {
            this.initializeTab();
            this.form.controls['cin'].reset();
            // Display SweetAlert success message
            Swal.fire({
              title: 'Client supprimé !',
              icon: 'success',
              timer: 2000,
              showConfirmButton:false
            }

            );
          },
          (error) => {
            console.error('Error deleting Client:', error);
          }
        );
      }
    });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDialogClientComponent } from './confirm-dialog-client.component';

describe('ConfirmDialogClientComponent', () => {
  let component: ConfirmDialogClientComponent;
  let fixture: ComponentFixture<ConfirmDialogClientComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmDialogClientComponent]
    });
    fixture = TestBed.createComponent(ConfirmDialogClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-confirm-dialog-client',
  templateUrl: './confirm-dialog-client.component.html',
  styleUrls: ['./confirm-dialog-client.component.css']
})
export class ConfirmDialogClientComponent {
  public title!: string;
  public content!: string;
  public color!: string;
  public confirmButton = "Confrmer";
  public cancelButton = "Annuler";
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.title = data.title ;
    this.content = data.content ;
    this.color = data.color ;
  }

}

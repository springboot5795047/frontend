import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClientService} from "../../../services/client.service";
import {LocationStrategy} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SweetAlert2LoaderService} from "@sweetalert2/ngx-sweetalert2";
import {MatTableDataSource} from "@angular/material/table";
import {Client} from "../../../models/Client";
import {AuthentificationService} from "../../../services/authentification.service";
import {jwtDecode} from 'jwt-decode';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dash-login',
  templateUrl: './dash-login.component.html',
  styleUrls: ['./dash-login.component.css']
})
export class DashLoginComponent implements OnInit {
  form!:FormGroup;

  initForm(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
    });
  }

  constructor(private router : Router,private AS: AuthentificationService, private locationStrategy: LocationStrategy,private swalService: SweetAlert2LoaderService) {}

  ngOnInit() {
    this.initForm();
  }

  login() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
    const data = {
      email: this.form.value.email,
      password: this.form.value.password
    };

    this.AS.signIn(data).subscribe(
      (x) => {
            console.log(x.data.token);
            localStorage.setItem("token",x.data.token);
        const decodedToken = jwtDecode(x.data.token);

        if (decodedToken && decodedToken.sub) {
          localStorage.setItem("email", decodedToken.sub);
        } else {
          console.error('Error decoding token or missing "sub" property.');
        }

        console.log(jwtDecode(x.data.token));
            this.router.navigate(['/ClientDash']);
      },
      (error) => {
        console.error('Error saving compte:', error);
        this.form.controls['name'].reset();
      }
    );
  }
  }
}

import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dash-nav',
  templateUrl: './dash-nav.component.html',
  styleUrls: ['./dash-nav.component.css']
})
export class DashNavComponent implements OnInit {
  name:String="";
  ngOnInit(): void {
    setTimeout(() => {
      // Function to read the value from localStorage
      this.name = localStorage.getItem("email") ?? "";
    }, 200);
  }
  constructor(private router: Router,){}
  logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("mail");
    this.router.navigate(['/Login']);
  }
}

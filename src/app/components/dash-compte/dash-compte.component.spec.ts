import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashCompteComponent } from './dash-compte.component';

describe('DashCompteComponent', () => {
  let component: DashCompteComponent;
  let fixture: ComponentFixture<DashCompteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DashCompteComponent]
    });
    fixture = TestBed.createComponent(DashCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject, map, Observable, startWith} from "rxjs";
import {Client} from "../../../../models/Client";
import {CompteService} from "../../../../services/compte.service";
import {ClientService} from "../../../../services/client.service";
import {LocationStrategy} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {SweetAlert2LoaderService} from "@sweetalert2/ngx-sweetalert2";
import {MatTableDataSource} from "@angular/material/table";
import {Compte} from "../../../../models/Compte";
import {Router} from "@angular/router";
import Swal from "sweetalert2";

@Component({
  selector: 'app-ajout-compte',
  templateUrl: './ajout-compte.component.html',
  styleUrls: ['./ajout-compte.component.css']
})
export class AjoutCompteComponent implements OnInit{
  form!:FormGroup;
  formcontrol = new FormControl('');
  clients!: Client[];
  filteroptionslist!: Observable<Client[]>
  private clientsSubject = new BehaviorSubject<Client[]>([]);
  clients$ = this.clientsSubject.asObservable();

  constructor(private CS: CompteService, private ClS: ClientService,private locationStrategy: LocationStrategy, private router : Router,private swalService: SweetAlert2LoaderService,private formBuilder: FormBuilder) {
    this.ClS.GetAllClient().subscribe((response) => {
      this.clients = response.data;
      console.log(this.clients);
    });
    this.loadData();
    this.form = this.formBuilder.group({
      client: ['', Validators.required],
      rib: [''],
      solde: [''],
    });
  }
  private loadData(): void {
    this.ClS.GetAllClient().subscribe(
      (response) => {
        this.clients = response.data;
        console.log(this.clients);

        // Notify subscribers (e.g., other methods) that the data is ready
        this.clientsSubject.next(this.clients);
      },
      (error) => {
        console.error('Error fetching client data:', error);
      }
    );
  }
  initForm(): void {
    this.form = new FormGroup({
      rib: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9]{22}$/)
      ]),
      client: new FormControl(null, [Validators.required]),
      solde: new FormControl(null, [Validators.required])
    });
  }

  ngOnInit() {
    this.initForm();
    this.clients$.subscribe((clients) => {
      console.log(clients); // Access clients when ready
      // Update filteroptionslist when clients$ emits a new value
      this.filteroptionslist = this.form.get('client')!.valueChanges.pipe(
        startWith(''),
        map(value => this._LISTFILTER(value || '', clients))
      );});
  }
  private _LISTFILTER(value: string, clients: Client[]): Client[] {
    const searchValue = value.toLocaleLowerCase();
    return clients.filter(option =>
      option.nom.toLocaleLowerCase().includes(searchValue) ||
      option.prenom.toLocaleLowerCase().includes(searchValue) ||
      option.cin.toString().toLocaleLowerCase().includes(searchValue)
    );
  }

  onsub(): void {
    const clientData = this.form.get('client')!.value;
    const ribControl = this.form.get('rib');
    this.ClS.getClientByCin(clientData).subscribe(response => {
        if (response.code == 200) {
          if (ribControl && ribControl.valid) {
            this.CS.getCompteByRib(ribControl.value).subscribe(response=> {
              if (response.code == 200) {
                // Client exists, show SweetAlert
                Swal.fire({
                  title: 'Compte existe déjà',
                  text: 'Le compte avec le numéro RIB spécifié existe déjà.',
                  icon: 'warning',
                  confirmButtonText: 'OK',
                });
                this.form.controls['rib'].reset();

                this.form.controls['solde'].reset();
              }
              else {
                const client: Client = {
                  cin: clientData,
                  nom: "",
                  prenom: "",
                };

                // Create the data object with all necessary information
                const data = {
                  rib: this.form.value.rib,
                  client: client,
                  solde: this.form.value.solde
                };
                console.log(data);
                console.log(this.form.get('client')!.value);
                this.CS.saveCompte(data).subscribe(
                  (x) => {
                    Swal.fire({
                        title: 'Compte ajouté avec succés !',
                        icon: 'success',
                        timer: 4000,
                        showConfirmButton: false
                      }
                    );
                    this.router.navigate(['/CompteDash']);
                  },
                  (error) => {
                    console.error('Error saving compte:', error);
                    this.form.controls['name'].reset();
                  }
                );
              }
            });

          }
          else {
            this.form.controls['rib'].reset();
            Swal.fire({
              title: 'Numéro RIB incorrecte !',
              text: 'Le RIB doit comporter exactement 22 caractéres .',
              icon: 'warning',
              confirmButtonText: 'OK',
              showConfirmButton: false
            });
          }
        }
        else {
          Swal.fire({
            title: 'Client n\'existe pas',
            text: 'Le client avec ce numéro CIN spécifié n\'existe pas.',
            icon: 'warning',
            confirmButtonText: 'OK',
          });
        }
    });

  }



}

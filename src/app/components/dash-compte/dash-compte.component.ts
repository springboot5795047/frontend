import {Component,  OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Client} from "../../../models/Client";
import {MatTableDataSource} from "@angular/material/table";
import {Compte} from "../../../models/Compte";
import {ClientService} from "../../../services/client.service";
import {LocationStrategy} from "@angular/common";
import {SweetAlert2LoaderService} from "@sweetalert2/ngx-sweetalert2";
import {CompteService} from "../../../services/compte.service";
import Swal from "sweetalert2";
import {Observable} from "rxjs";


@Component({
  selector: 'app-dash-compte',
  templateUrl: './dash-compte.component.html',
  styleUrls: ['./dash-compte.component.css'],
})
export class DashCompteComponent implements OnInit{
  form!:FormGroup;
  comptes!: Compte[];
  compte!: Compte;
  displayedColumns: string[] = ['rib','cinClient','nomClient','solde','actions'];
  clients!: Client[];
  dataSource!: MatTableDataSource<Compte>;

  formcontrol = new FormControl('');

  filteroptionslist!: Observable<Client[]>



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private CS: CompteService, private ClS: ClientService,private locationStrategy: LocationStrategy,private swalService: SweetAlert2LoaderService) {
  }

  initializeTab() {
    this.CS.GetAllCompte().subscribe((response) => {
      this.comptes = response.data;
      this.dataSource.data = this.comptes;
    });
  }
  initForm(): void {
    this.form = new FormGroup({
      rib: new FormControl(null, [Validators.required]),
      ribDisabled: new FormControl({value: null, disabled: true}, [Validators.required]),
      soldeEdit: new FormControl(null, [Validators.required])
    });
  }
  ngOnInit() {
    this.dataSource = new MatTableDataSource<Compte>();
    this.initializeTab();
    this.initForm();

  }
  deleteCompte(rib: string) {
    Swal.fire({
      title: 'Vous êtes sûr ?',
      text: 'Voulez-vous vraiment supprimer ce compte ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',
    }).then((result) => {
      if (result.isConfirmed) {
        this.CS.deleteCompte(rib).subscribe(

          (response) => {
            this.initializeTab();
            this.form.controls['rib'].reset();
            // Display SweetAlert success message
            Swal.fire({
                title: 'Compte supprimé !',
                icon: 'success',
                timer: 2000,
                showConfirmButton:false
              }

            );
            console.log(response);
            console.log(rib);
          },
          (error) => {
            console.error('Error deleting Compte:', error);
          }
        );
      }
    });
  }
  open(rib:string){
    this.CS.getCompteByRib(rib).subscribe(compte => {
      this.compte = compte.data;
      console.log(this.compte);
      this.form.controls['soldeEdit'].setValue(this.compte.solde);
      this.form.controls['rib'].setValue(this.compte.rib);
      this.form.controls['ribDisabled'].setValue(this.compte.rib);
    });
  }
  update() {
    this.form.markAllAsTouched();
    const data = {
      rib: this.form.value.rib,
      solde: this.form.value.soldeEdit,
    client:this.compte.client
    };
    if (this.form.valid) {
      this.CS.updateCompte(data, data.rib).subscribe(() => {
          this.locationStrategy.back();
          Swal.fire({
              title: 'Compte mis à jour avec succés !',
              icon: 'success',
              timer: 7000,
              showConfirmButton: false
            }
          );
          window.location.reload();
        },
        (error) => {
          this.form.controls['rib'].reset();
          this.form.controls['ribDisabled'].reset();
          this.form.controls['soldeEdit'].reset();

        }
      );
    }
  }
}




import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashSideComponent } from './dash-side.component';

describe('DashSideComponent', () => {
  let component: DashSideComponent;
  let fixture: ComponentFixture<DashSideComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DashSideComponent]
    });
    fixture = TestBed.createComponent(DashSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

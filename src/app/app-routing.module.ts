import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashHomeComponent} from "./components/dash-home/dash-home.component";
import {DashClientComponent} from "./components/dash-client/dash-client.component";
import {DashCompteComponent} from "./components/dash-compte/dash-compte.component";
import {AjoutCompteComponent} from "./components/dash-compte/ajout-compte/ajout-compte.component";
import {DashLoginComponent} from "./components/dash-login/dash-login.component";
import {DashRegisterComponent} from "./components/dash-register/dash-register.component";
import {DashCompteClientComponent} from "./components/dash-compte-client/dash-compte-client.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
  {
    path: 'DashHome',
    pathMatch: 'full',
    component: DashHomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'ClientDash',
    pathMatch: 'full',
    component: DashClientComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'CompteDash',
    pathMatch: 'full',
    component: DashCompteComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'AddCompte',
    pathMatch: 'full',
    component: AjoutCompteComponent,
    canActivate: [AuthGuard]
  },
    {
      path: 'CompteByClient/:cin',
      pathMatch: 'full',
      component: DashCompteClientComponent,
      canActivate: [AuthGuard]
    },
  {
    path: 'Login',
    pathMatch: 'full',
    component: DashLoginComponent,
  },
  {
    path: 'Register',
    pathMatch: 'full',
    component: DashRegisterComponent,
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: "Login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
